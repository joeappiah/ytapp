import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import VideoModel from '@/models/VideoModel';
import {
  GETTER_VIDEO_LIST_DATA,
  GETTER_SELECTED_VIDEO,
  MUTATION_VIDEO_LIST,
  MUTATION_SELECTED_VIDEO,
  ACTION_GET_VIDEO_LIST,
} from './constants/storeConstants';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    videoList: [],
    selectedVideo: {},
  },

  plugins: [createPersistedState({
    storage: {
      getItem: key => window.sessionStorage.getItem(key),
      setItem: (key, value) => window.sessionStorage.setItem(key, value),
      removeItem: key => window.sessionStorage.removeItem(key),
    },
  })],

  getters: {
    [GETTER_VIDEO_LIST_DATA]: state => state.videoList,
    [GETTER_SELECTED_VIDEO]: state => state.selectedVideo,
  },
  mutations: {
    [MUTATION_VIDEO_LIST](state, data) {
      state.videoList = data.items.filter(video => video.snippet.title !== 'Deleted video');
    },
    [MUTATION_SELECTED_VIDEO](state, data) {
      state.selectedVideo = data;
    },
  },
  actions: {
    [ACTION_GET_VIDEO_LIST]({ commit }) {
      return VideoModel.getVideos()
        .then(data => data.json())
        .then((response) => {
          commit(MUTATION_VIDEO_LIST, response);
        });
    },
  },
});

export default store;
