// Getters
export const GETTER_VIDEO_LIST_DATA = 'video list';
export const GETTER_SELECTED_VIDEO = 'selected video';

// Mutations
export const MUTATION_VIDEO_LIST = 'video list data';
export const MUTATION_SELECTED_VIDEO = 'set selected video';

// Actions
export const ACTION_GET_VIDEO_LIST = 'get video list data form youtube';
