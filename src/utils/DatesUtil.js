class DatesUtil {
  static formatDate(dateString) {
    const date = new Date(dateString).toDateString();
    const newDateString = date.split(' ').splice(1, 3);
    newDateString[1] += ',';

    return newDateString.join(' ');
  }
}

export default DatesUtil;
